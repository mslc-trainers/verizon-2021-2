package com.mslc.trainings.reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringReactiveTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringReactiveTrainingApplication.class, args);
	}

}
