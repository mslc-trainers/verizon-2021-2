package com.mslc.trainings.reactive.dao;


import com.mslc.trainings.reactive.model.Customer;
import com.mslc.trainings.reactive.model.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Component
public class CustomerDAO {


    @Autowired
    CustomerRepository customerRepository;

    public Flux<Customer> getCustomers() {

        Flux<Customer> customers = customerRepository.findAll();


        return customers;

    }

    public Mono<Customer> getCustomerById(String id) {

        Mono<Customer> customerMono = customerRepository.findById(id);

        return customerMono;

    }


}
