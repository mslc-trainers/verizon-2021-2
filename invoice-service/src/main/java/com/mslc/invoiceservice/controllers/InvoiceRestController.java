package com.mslc.invoiceservice.controllers;


import com.mslc.invoiceservice.valueobjects.Customer;
import com.mslc.invoiceservice.valueobjects.CustomerAddress;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

@RestController
public class InvoiceRestController {


    WebClient webClient = WebClient.create("http://localhost:8080");


    @GetMapping(path = "/invoice-customers")
    public Flux<Customer> handleGetInvoiceCustomers() {


        Flux<Customer> customerFlux = webClient
                .get()
                .uri("/customers")
                .retrieve()
                .bodyToFlux(Customer.class);
        return customerFlux;
//
//        RestTemplate template = new RestTemplate();
//
//        ResponseEntity<List<Customer>> customers = template.exchange("http://localhost:8080/customers", HttpMethod.GET
//                , null, new ParameterizedTypeReference<List<Customer>>() {
//                });
//
//        List<Customer> customerList = customers.getBody();


    }


    private Mono<Customer> getCustomerById(String customerId) {

        Mono<Customer> customerMono = webClient
                .get()
                .uri("/customers/{customerId}", customerId)
                .retrieve()
                .bodyToMono(Customer.class);

        return customerMono;
    }


    @GetMapping(path = "/invoice-customers/{customerId}")
    public Mono<Customer> handleGetInvoiceCustomerById(@PathVariable String customerId) {


        return this.getCustomerById(customerId);

    }


    private Flux<CustomerAddress> getPostalAddresses(String customerId) {

        return
                webClient
                        .get()
                        .uri(uriBuilder -> {
                            URI uri = uriBuilder
                                    .path("/customers/{customerId}/addresses")
                                    .queryParam("type", "P")
                                    .build(customerId);

                            return uri;
                        })
                        .retrieve()
                        .bodyToFlux(CustomerAddress.class);

    }

    private Flux<CustomerAddress> getDeliverAddresses(String customerId) {

        return
                webClient
                        .get()
                        .uri(uriBuilder -> {
                            URI uri = uriBuilder
                                    .path("/customers/{customerId}/addresses")
                                    .queryParam("type", "D")
                                    .build(customerId);

                            return uri;
                        })
                        .retrieve()
                        .bodyToFlux(CustomerAddress.class);

    }

    @GetMapping(path = "/invoice-customer-addresses")
    public Flux<CustomerAddress> handleGetCustomerAddressesByIdAndType() {

        String customerId = "103";
        String type = "P";

        Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(customerId);
        Flux<CustomerAddress> deliveryAddresses = this.getDeliverAddresses(customerId);

        return
                Flux.merge(postalAddresses, deliveryAddresses);


    }

    @GetMapping(path = "/demonstrate-complex-use-case")
    public Flux<Customer> handleComplexUseCase() {

        List<String> customerIds = Arrays.asList("101", "102", "103");

        Flux<Customer> customerFlux =
                Flux.fromIterable(customerIds)
                        .flatMap(x -> {

                            Mono<Customer> customerMono =
                                    this.getCustomerById(x)
                                            .zipWhen(cust -> {

                                                Flux<CustomerAddress> postalAddresses = this.getPostalAddresses(cust.getId());
                                                Flux<CustomerAddress> deliverAddresses = this.getDeliverAddresses(cust.getId());
                                                Flux<CustomerAddress> allAddresses =
                                                        Flux.merge(postalAddresses, deliverAddresses);

                                                Mono<List<CustomerAddress>> addressList = allAddresses.collectList();

                                                return addressList;
                                            })
                                            .map(y -> {
                                                Customer customer = y.getT1();
                                                List<CustomerAddress> addressList = y.getT2();
                                                customer.setAddresses(addressList);
                                                return customer;
                                            });
                            return customerMono;
                        });


        return customerFlux;
    }


}
