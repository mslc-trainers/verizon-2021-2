package com.mslc.invoiceservice.valueobjects;


import lombok.Data;

import java.util.List;

@Data
public class Customer {

    private String id;
    private String name;
    private List<CustomerAddress> addresses;
}
