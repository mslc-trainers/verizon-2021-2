package com.mslc.invoiceservice.valueobjects;


import lombok.Data;

@Data
public class CustomerAddress {

    private String customerAddressId;

    private String customerId;
    private String line1;
    private String line2;
    private String city;
    private String type;
}
