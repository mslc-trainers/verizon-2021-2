package com.mslc.trainings.reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class MoreWithFlux {


    private static List<String> clients =
            Arrays.asList("BoA", "Verizon", "BNP Paribas", "Morgan Stanley", "JP Morgan");

    @Test
    public void when_concat_with() {
        Flux<String> stringFlux =
                Flux.fromIterable(clients)
                        .concatWith(Flux.just("New Client"));
        stringFlux
                .log()
                .subscribe();
    }

    @Test
    public void when_flux_concat() throws InterruptedException {

        Flux<String> f1 = Flux
                .just("101", "102", "103")
                .delayElements(Duration.ofSeconds(1));

        Flux<String> f2 = Flux
                .just("201", "202", "203")
                .delayElements(Duration.ofSeconds(1));

        Flux<String> f3 = Flux.concat(f1, f2);

        f3
                .log()
                .subscribe();


        Thread.sleep(7000);
    }

    @Test
    public void when_flux_merge() throws InterruptedException {

        Flux<String> f1 = Flux
                .just("101", "102", "103")
                .delayElements(Duration.ofSeconds(1));

        Flux<String> f2 = Flux
                .just("201", "202", "203")
                .delayElements(Duration.ofSeconds(1));

        Flux<String> f3 = Flux.merge(f1, f2);

        f3
                .log()
                .subscribe();


        Thread.sleep(7000);
    }

    @Test
    public void when_zip() throws InterruptedException {
        Flux<Long> longFlux = Flux
                .interval(Duration.ofSeconds(1));
        Flux<String> stringFlux = Flux.just("101", "102", "103");
//        Flux<Tuple2<Long, String>> tuple2Flux = longFlux
//                .zipWith(stringFlux);
//
//        tuple2Flux.subscribe(x -> {
//
//            System.out.println(x.getT1() + " -- " + x.getT2());
//        });

        Flux<String> tuple2Flux = longFlux
                .zipWith(stringFlux, (x, y) -> {
                    return x + " > " + y;
                });
        tuple2Flux.subscribe(x -> {
            System.out.println(x);
        });
        Thread.sleep(5000);
    }


}
