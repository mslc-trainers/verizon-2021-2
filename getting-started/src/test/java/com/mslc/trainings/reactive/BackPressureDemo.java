package com.mslc.trainings.reactive;

import org.junit.jupiter.api.Test;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;

public class BackPressureDemo {


    @Test
    public void back_pressure_demo() {

        Flux<Integer> integerFlux =
                Flux.range(0, 15)
                        .log();
        integerFlux.subscribe(new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnSubscribe(Subscription subscription) {
                subscription.request(6);
            }
            @Override
            protected void hookOnNext(Integer value) {
                if (value > 0 && value % 5 == 0) {
                    System.out.println("requesting for another 5 elements");
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    super.request(5);
                }
            }
        });


    }
}
