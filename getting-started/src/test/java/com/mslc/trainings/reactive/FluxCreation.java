package com.mslc.trainings.reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class FluxCreation {


    @Test
    public void when_just() {
        Flux<String> clients = Flux
                .just("BoA", "Verizon", "BNP Paribas", "Morgan Stanley", "JP Morgan")
                .log();

//        StepVerifier
//                .create(clients)
//                .expectSubscription()
//                .expectNext("BoA")
//                .expectNext("Verizon")
//                .expectNext("BNP Paribas")
//                .expectNext("Morgan Stanley")
//                .expectNext("JP Morgan")
//                .expectComplete()
//                .verify();


//        StepVerifier
//                .create(clients)
//                .expectSubscription()
//                .expectNextCount(3)
//                .expectNextCount(2)
//                .expectComplete()
//                .verify();

        StepVerifier
                .create(clients)
                .expectSubscription()
                .expectNextCount(6)
                .verifyComplete();

//        Mono<List<String>>  listMono = clients.collectList();
//
//        List<String> stringList = listMono.block();
//        stringList.forEach(System.out::println);



//        listMono
//                .subscribe(x -> {
//                    x.forEach(System.out::println);
//                });



    }

    @Test
    public void when_fromIterable() {
        Flux
                .fromIterable(Arrays
                        .asList("BoA", "Verizon", "BNP Paribas", "Morgan Stanley", "JP Morgan"))
                .log()
                .subscribe();
    }

    @Test
    public void when_interval() throws InterruptedException {
        Flux
                .interval(Duration.ofSeconds(1))
                .log()
                .subscribe();
        Thread.sleep(5000);
    }

    @Test
    public void when_fromPublisher() throws InterruptedException {


        Flux.from(Flux.just("Verizon"));


    }

    @Test
    public void when_fromArray() throws InterruptedException {

        Flux.fromArray(new String[]{"BoA"});

    }

    @Test
    public void when_range() throws InterruptedException {

        Flux
                .range(5, 10)
                .log()
                .subscribe();

    }


}
