package com.mslc.trainings.reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class ColdAndHotReactiveStreams {


    @Test
    public void when_cold() throws InterruptedException {


        Flux<String> stringFlux =
                Flux.just("a", "b", "c", "d", "e")
                        .delayElements(Duration.ofSeconds(1));

        stringFlux
                .subscribe(x -> {
                    System.out.println("     " + x + " Subscriber 1");
                });


        stringFlux
                .subscribe(x -> {
                    System.out.println(x + " Subscriber 2");
                });


        Thread.sleep(7000);


    }

    @Test
    public void when_hot_stream() throws InterruptedException {
        Flux<String> stringFlux =
                Flux.just("a", "b", "c", "d", "e")
                        .delayElements(Duration.ofSeconds(1));
        ConnectableFlux<String> connectableFlux =
                stringFlux.publish();
        connectableFlux.connect();

        connectableFlux
                .subscribe(x -> {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("      " + x + " Subscriber 1 " + Thread.currentThread().getName());
                });

        Thread.sleep(2000);

        System.out.println(" ******** ");
        connectableFlux
                .subscribe(x -> {
                    System.out.println(x + " Subscriber 2 " + Thread.currentThread().getName());
                });
        Thread.sleep(10000);


    }

}
