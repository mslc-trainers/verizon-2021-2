package com.mslc.trainings.reactive;

import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.sql.SQLException;

public class ErrorHandlingInFlux {


    @Test
    public void when_error_consumer() {

        Flux<Integer> integerFlux = Flux
                .range(0, 6)
                .map(x -> {
                    if (x == 2) {
                        throw new RuntimeException("Deliberate for demo...");
                    }
                    return x + 1;
                });

        StepVerifier
                .create(integerFlux)
                .expectSubscription()
                .expectNext(1)
                .expectNext(2)
                .expectErrorMessage("Deliberate for demo...")
//                .expectError()
                .verify();






//
//        integerFlux
//                .subscribe(
//                        x -> {
//                            System.out.println(x);
//                        },
//                        x -> {
//                            System.out.println("Error Consumer : " + x.getMessage());
//                        },
//                        () -> {
//                            System.out.println("Completed....");
//                        }
//                );

    }


    @Test
    public void when_on_error_resume() {

        Flux<Integer> integerFlux = Flux
                .range(0, 6)
                .map(x -> {
                    if (x == 2) {
                        throw new RuntimeException("Deliberate for demo...");
                    }
                    return x + 1;
                })
                .onErrorResume(x -> {
                    System.out.println("The Exception is : " + x.getMessage());

                    return
                            Flux.just(50, 60, 70);

                });


        integerFlux
                .subscribe(
                        x -> {
                            System.out.println(x);
                        },
                        x -> {
                            System.out.println("Error Consumer : " + x.getMessage());
                        },
                        () -> {
                            System.out.println("Completed....");
                        }
                );

    }

    @Test
    public void when_on_error_continue() {

        Flux<Integer> integerFlux = Flux
                .range(0, 6)
                .map(x -> {
                    if (x == 2) {
                        throw new RuntimeException("Deliberate for demo...");
                    }
                    return x;
                })
                .onErrorContinue((x, y) -> {

                    System.out.println("The Exception is : " + x.getMessage() + " for element : " + y);
                });

        integerFlux
                .subscribe(
                        x -> {
                            System.out.println(x);
                        },
                        x -> {
                            System.out.println("Error Consumer : " + x.getMessage());
                        },
                        () -> {
                            System.out.println("Completed....");
                        }
                );

    }


    @Test
    public void when_on_error_return() {

        Flux<Integer> integerFlux = Flux
                .range(0, 6)
                .map(x -> {
                    if (x == 2) {
                        throw new RuntimeException("Deliberate for demo...");
                    }
                    return x;
                }).onErrorReturn(25);

        integerFlux
                .subscribe(
                        x -> {
                            System.out.println(x);
                        },
                        x -> {
                            System.out.println("Error Consumer : " + x.getMessage());
                        },
                        () -> {
                            System.out.println("Completed....");
                        }
                );

    }


    @Test
    public void when_on_error_map() {

        Flux<Integer> integerFlux = Flux
                .range(0, 6)
                .map(x -> {
                    if (x == 2) {
                        throw new RuntimeException("Deliberate for demo...");
                    }
                    return x;
                }).onErrorMap(x -> new SQLException("SQL Exception"));

        integerFlux
                .subscribe(
                        x -> {
                            System.out.println(x);
                        },
                        x -> {
                            System.out.println("Error Consumer : " + x.getMessage());
                        },
                        () -> {
                            System.out.println("Completed....");
                        }
                );

    }

}
