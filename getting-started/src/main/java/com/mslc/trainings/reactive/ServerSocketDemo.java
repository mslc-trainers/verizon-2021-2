package com.mslc.trainings.reactive;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketDemo {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(7001);

        System.out.println("Listening to incoming connection in thread : " + Thread.currentThread().getName());
        Socket s = serverSocket.accept();

        System.out.println("Received incoming connection...");




    }
}
