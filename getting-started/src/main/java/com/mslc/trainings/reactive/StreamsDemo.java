package com.mslc.trainings.reactive;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamsDemo {

    public static void main(String[] args) {


        Stream<String> stringStream = Stream.of("BNP Paribas", "JP Morgan", "Morgan Stanley", "Verizon", "BoA");

        long start = System.currentTimeMillis();

        List<String> mappedStrings = stringStream
                .parallel()
                .map(x -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Processing : " + x + " in : " + Thread.currentThread().getName());
                    return x.toLowerCase(Locale.ROOT);
                })
                .filter(x -> x.contains("morgan"))
                .collect(Collectors.toList());


        long end = System.currentTimeMillis();
        System.out.println(mappedStrings + " completed in " + (end-start));


    }
}
