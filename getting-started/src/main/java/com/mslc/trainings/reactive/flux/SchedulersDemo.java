package com.mslc.trainings.reactive.flux;

import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadFactory;

public class SchedulersDemo {


    public static void main(String[] args) throws InterruptedException {

//        Scheduler scheduler1 = Schedulers.newParallel("yabadabadoo", 16, true);
        Scheduler scheduler1 = Schedulers.newBoundedElastic(300, 500, "my-bounded-elastic");
//        Scheduler scheduler1 = Schedulers.from


//        Schedulers.newParallel(16, new ThreadFactory() {
//            @Override
//            public Thread newThread(Runnable r) {
//
//                Thread t = new Thread(r);
//
//                return t;
//
//            }
//        });
        Scheduler scheduler2 = Schedulers.parallel();
        System.out.println(scheduler1.hashCode() + " -- " + scheduler2.hashCode());

        int numberOfTasks = 16;

        CountDownLatch latch = new CountDownLatch(numberOfTasks);
        long start = System.currentTimeMillis();


        for (int i = 0; i < numberOfTasks; i++) {
            final int j = i;
            scheduler1.schedule(new Runnable() {
                @Override
                public void run() {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    System.out.println("Task : " + j + " is executed in thread : " + Thread.currentThread().getName());

                    latch.countDown();

                }
            });
        }

        latch.await();
        long end = System.currentTimeMillis();
        System.out.println("The total time taken is : " + (end - start));
//        scheduler1.dispose();

        System.out.println("Exiting Main...");


    }
}
