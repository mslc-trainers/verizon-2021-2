package com.mslc.trainings.reactive.flux;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class FluxDemo {


    public static void main(String[] args) throws InterruptedException {


//        ExecutorService executorService = Executors.newFixedThreadPool(10);
        ExecutorService executorService = Executors.newCachedThreadPool();

        Scheduler scheduler = Schedulers.parallel();

        Stream<String> stringStream = Stream.of("V1", "V2");

        Flux<String> stringFlux = Flux.just("BNP Paribas", "JP Morgan", "Morgan Stanley", "Verizon", "BoA");

        ParallelFlux<String> mappedValues = stringFlux
                .parallel()
                .runOn(Schedulers.boundedElastic())
                .map(x -> {

//                    System.out.println(x + "::::  being processed in thread : " + Thread.currentThread().getName());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return x.toLowerCase(Locale.ROOT);
                })
                .filter(x -> x.contains("morgan"));

//        System.out.println("Have reached : " + Thread.currentThread().getName());

        mappedValues.subscribe(x -> {
//            System.out.println("       Value : " + x + " thread : " + Thread.currentThread().getName());
        }, error -> {

//            System.out.println("error : " + error.getMessage());

        }, () -> {
//            System.out.println("Completed...");
        });
        Thread.sleep(10000);

//        Mono<String> stringMono = Mono.just("Verizon");
//
//        stringMono.subscribe(x -> {
//            System.out.println(x);
//        });



    }
}
