package com.mslc.trainings.reactive;

public class UsingThreads {


    public static void main(String[] args) {


        // Common - DataStructure

        new Thread() {

            @Override
            public void run() {

                // RestTemplate : /customers?city=Mumbai (3 seconds)
            }
        }.start();


        new Thread() {

            @Override
            public void run() {

                // RestTemplate : /customer-addresses (2 seconds)
            }
        }.start();


        // 5 seconds.


    }
}
