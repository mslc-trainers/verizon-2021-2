package com.mslc.trainings.reactive;

public class MoreOnThreads {

    public static void main(String[] args) throws InterruptedException {
        new Thread() {

            {
//                setDaemon(true);

            }

            @Override
            public void run() {
                System.out.println("In Thread : " + Thread.currentThread().getName());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
        System.out.println("This is the last executable line in : "  + Thread.currentThread().getName());
    }
}
