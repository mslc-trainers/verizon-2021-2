package com.mslc.trainings.reactive.flux;

import reactor.core.publisher.Flux;
import reactor.core.publisher.ParallelFlux;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Stream;

public class FluxEventsDemo {


    public static void main(String[] args) throws InterruptedException {


        Flux<String> stringFlux = Flux.just("BNP Paribas", "JP Morgan", "Morgan Stanley", "Verizon", "BoA");
        stringFlux
                .parallel()
                .runOn(Schedulers.single())
                .map(x -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return x.toLowerCase(Locale.ROOT);
                })
//                .filter(x -> x.contains("morgan"))
                .log()
                .subscribe();


//        mappedValues.subscribe(x -> {
////            System.out.println("       Value : " + x + " thread : " + Thread.currentThread().getName());
//        });
        Thread.sleep(10000);


    }
}
