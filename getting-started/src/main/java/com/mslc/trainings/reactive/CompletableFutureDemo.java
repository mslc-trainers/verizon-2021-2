package com.mslc.trainings.reactive;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class CompletableFutureDemo {

    public static void main(String[] args) throws InterruptedException, ExecutionException {


        int threads = ForkJoinPool.commonPool().getParallelism();
        System.out.println("The total number of threads in the common pool : " + threads);


        CompletableFuture<String> cf = CompletableFuture.supplyAsync(() -> {
            System.out.println("Thread : " + Thread.currentThread().getName());
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "{'id' : 101, 'name' : 'Verizon'}";
        });


        cf.whenComplete((x, y) -> {

            if (y == null) {
                System.out.println("Received value : " + x + " in thread : " + Thread.currentThread().getName());
            } else {
                System.out.println("Exception occurred : " + y.getMessage());
            }

        });

        System.out.println("This is : " + Thread.currentThread().getName());


        Thread.sleep(7000);

//


    }
}
