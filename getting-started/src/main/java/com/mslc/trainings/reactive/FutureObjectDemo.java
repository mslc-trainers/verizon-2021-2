package com.mslc.trainings.reactive;

import java.util.concurrent.*;

public class FutureObjectDemo {

    public static void main(String[] args) {


        Executor threadPoolExecutor = new ThreadPoolExecutor(10, 50,
                60, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(100));

        ExecutorService service = Executors.newFixedThreadPool(100);

        Future<String> future1 = service.submit(new Callable<String>() {

            @Override
            public String call() throws Exception {

                // RestTemplate : /customers?city=Mumbai (3 seconds)

                Thread.sleep(5000);

                if (true) {
//                    throw new RuntimeException("Some error occurred");
                }

                return "{'id' : 101, 'name' : 'Verizon'}";
            }
        });

        Future<String> future2 = service.submit(new Callable<String>() {

            @Override
            public String call() throws Exception {

                // RestTemplate : /customer-addresses (2 seconds)
                Thread.sleep(2000);
                return "{'addressId' : 102, 'city' : 'Hyderabad'}";
            }
        });

        try {

            System.out.println("Trying to get the value from api 1");
            String value1 = future1.get();
            System.out.println("value received from api 1 : " + value1);

            String value2 = future2.get();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }
}
